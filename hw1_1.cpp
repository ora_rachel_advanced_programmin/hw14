#include "queue.h"
#include <iostream>
#pragma once


bool isEmpty(queue *q)
{
	return(q->num == 0);
}



void initQueue(queue *q, int maxSize)
{
	q->num = 0;
	q->max_size = maxSize;
	q->myArray = new int[maxSize];
}



void cleanQueue(queue *q)
{
	delete[] q->myArray;
}



void enqueue(queue *q, int newValue)
{
	q->num += 1;
	q->myArray[q->num] = newValue;
}




int dequeue(queue *q)
{
	int temp = 0;
	int count = 1;
	q->num -= 1;            //There's going to be one less num in the array
	temp = q->myArray[0];
	while (!isEmpty)
	{
		q->myArray[count] = q->myArray[count + 1];
		count += 1;
	}
	return q->myArray[q->num];
}
